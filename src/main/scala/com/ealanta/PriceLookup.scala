package com.ealanta

import com.ealanta.Item._

/**
 * Used to lookup an item's price in pence.
 * best to separate the Item from its price.
 * @author davidhay
 */
trait Pricing {
  def lookupPrice(item:Item):Int
}

/**
 * Simple implementation of Pricing trait with fixed values.
 * @author davidhay
 */
object PricingUtils extends Pricing {
  
  def lookupPrice(item:Item):Int = {
    item.priceInPence
  }
  
}