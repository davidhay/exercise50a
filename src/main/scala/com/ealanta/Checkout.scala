package com.ealanta

import Item._

/**
 * Used to calculate the cost of a list/basket of shopping items
 * @author davidhay
 */
object Checkout {

  type Basket = List[Item]
  
  /**
   * Gets the total for a Basket of Items
   */
/**
   * Buy One, Get One Free.
   */
  def bogof(number: Int): Int = {
    val free = number / 2;
    val notFree = number - free;
    notFree
  }

  /**
   * Three For Two.
   */
  def threeForTwo(number: Int): Int = {
    val free = (number / 3)
    val notFree = number - free
    notFree
  }

  /**
   * Gets the total for a Basket of Items
   */
  def total(items: Basket): Int = {

    //get the counts for each item in the basket
    val itemCounts:Map[Item,Int] = items.groupBy { item => item } mapValues { _.size }

    /**
     * This method calculates the per item type subtotal
     * based on number of items, the unit cost of the item and the 'number of items' modifying formula
     */
    def subTotalItem(number: Int, unit: Int, modifier: Int => Int) = modifier(number) * unit;

    //Work out the subtotal for each item - each item can have a different formula
    val itemSubTotals = for {
      (item, number) <- itemCounts
    } yield {
      val unit = PricingUtils.lookupPrice(item)
      val afterOffer = item match {
        case apple:Item if Item.isApple(item)  => subTotalItem(number, unit, bogof)
        case orange:Item if Item.isOrange(item) => subTotalItem(number, unit, threeForTwo)
        case _      => subTotalItem(number, unit, n => n)
      }
      (item, afterOffer)
    }
    
    //add all item subTotals together
    val total = itemSubTotals.values.foldLeft(0)(_+_)
    total
  }

  /**
   * Gets the total for a list of tokens that represent items by their name.
   * Ignores tokens that do not match the name of any items.
   */
  def apply(tokens:List[String]):Int = {
    val items = for {
      token <- tokens
      item <- Item.lookup(token)
    } yield {
      item
    }
    total(items)
  }
  
}