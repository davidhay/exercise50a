package com.ealanta

case class Item(val name:String,val priceInPence:Int)

object Item {
 
  private val orange = new Item("Orange",25)
  private val apple = new Item("Apple",60)
  private val Items = List(apple,orange)
  
  def lookup(name:String):Option[Item] = Items.find(p => p.name == name)
  
  def isApple(p:Item):Boolean = apple.name == p.name
  def isOrange(p:Item):Boolean = orange.name == p.name
  
}