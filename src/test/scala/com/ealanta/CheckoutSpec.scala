package com.ealanta

import org.scalatest.FunSpec
import org.scalatest.GivenWhenThen
import org.scalatest.Matchers

/**
 * Tests for the Checkout
 * @author davidhay
 */
class CheckoutSpec extends FunSpec with GivenWhenThen with Matchers {

  describe("A Checkout") {

    describe("Should be able to calculate offers correctly") {
      describe("Should be able to calculate with BOGOF correctly") {
        it("Should ask you pay for 0 items when you have 0 on BOGOF") {
          Given("you have 0 items")
          val items = 0;
          
          When("you factor in BOGOG")
          val toPay = Checkout.bogof(items);
          
          Then("you have to pay for 0")
          toPay should equal(0)
        }
        it("Should ask you pay for 1 items when you have 1 on BOGOF") {
          Given("you have 1 items")
          val items = 1;
          
          When("you factor in BOGOG")
          val toPay = Checkout.bogof(items);
          
          Then("you have to pay for 1")
          toPay should equal(1)
        }
        it("Should ask you pay for 3 items when you have 5 on BOGOF") {
          Given("you have 3 items")
          val items = 3;
          
          When("you factor in BOGOG")
          val toPay = Checkout.bogof(items);
          
          Then("you have to pay for 2")
          toPay should equal(2)
        }
      }
      describe("Should be able to calculate with 3for2 correctly") {
        it("Should ask you pay for 0 items when you have 0 on 3for2") {
          Given("you have 0 items")
          val items = 0;
          
          When("you factor in 3for2")
          val toPay = Checkout.bogof(items);
          
          Then("you have to pay for 0")
          toPay should equal(0)
        }
        it("Should ask you pay for 1 items when you have 1 on 3for2") {
          Given("you have 1 items")
          val items = 1;
          
          When("you factor in 3for2")
          val toPay = Checkout.threeForTwo(items);
          
          Then("you have to pay for 1")
          toPay should equal(1)
        }
        it("Should ask you pay for 3 items when you have 3 on 3for2") {
          Given("you have 3 items")
          val items = 3;
          
          When("you factor in 3for2")
          val toPay = Checkout.threeForTwo(items);
          
          Then("you have to pay for 2")
          toPay should equal(2)
        }
        it("Should ask you pay for 5 items when you have 7 on 3for2") {
          Given("you have 7 items")
          val items = 7;
          
          When("you factor in 3for2")
          val toPay = Checkout.threeForTwo(items);
          
          Then("you have to pay for 5")
          toPay should equal(5)
        }
      }
    }
    describe("should be able to total a list of tokens representing items") {
      it("should correctly handle an empty token list") {
        Given("an empty list of tokens")
        val input = List()

        When("I calculate the total")
        val total = Checkout(input)

        Then("the result should be 0 pence")
        total should equal(0)
      }
      describe("should correctly compute the cost of a single valid token") {
        it("should correct compute the cost an Apple") {
          Given("the list 'Apple'")
          val input = List("Apple")

          When("I calculate the total")
          val total = Checkout(input)

          Then("the result should be 60 pence")
          total should equal(60)
        }
        it("should correct compute the cost an Orange") {
          Given("the list 'Orange'")
          val input = List("Orange")

          When("I calculate the total")
          val total = Checkout(input)

          Then("the result should be 25 pence")
          total should equal(25)
        }
      }
      describe("should correctly compute the cost of multiple valid tokens") {
        it("should correct compute the cost an 3 Apples and 2 Oranges") {
          Given("the list 'Apple','Orange','Apple','Orange','Apple'")
          val input = List("Apple", "Orange", "Apple", "Orange", "Apple")

          When("I calculate the total")
          val total = Checkout(input)

          Then("the result should be 230 pence (minus the cost of 1 free apple) : 170")
          total should equal(230-60)
        }
        it("should correct compute the cost an 3 Oranges and 2 Apples") {
          Given("the list 'Orange','Apple','Orange','Apple','Orange'")
          val input = List("Orange", "Apple", "Orange", "Apple", "Orange")

          When("I calculate the total")
          val total = Checkout(input)

          Then("the result should be 195 pence (minus the cost of 1 free orange and 1 fee apple) : 110p")
          total should equal(110)
        }
      }
      describe("should correctly compute the cost of multiple valid tokens ignoring invalid tokens") {
        it("should correct compute the cost an 1 Apple and 1 Orange") {
          Given("the list 'XXX','Orange','Apple','YYY','ZZZ'")
          val input = List("XXX", "Orange", "Apple", "YYY", "ZZZ")

          When("I calculate the total")
          val total = Checkout(input)

          Then("the result should be 85 pence")
          total should equal(85)
        }
        it("should correctly ignore tokens which are not a case sensitive match for Item names") {
          Given("the list 'apple','Apple','apple'")
          val input = List("apple", "Apple", "apple")

          When("I calculate the total")
          val total = Checkout(input)

          Then("the result should be 60 pence")
          total should equal(60)
        }
      }
    }
  }

}